import java.util.ArrayList;

import org.newdawn.slick.SlickException;

/**
 * Interface that allows Entities (I really should have called them GameObjects) to shoot
 * Potential to be updated later with shot direction and speed 
 */
public interface Shooter {

	/** creates a new projectile with the targets being enemies or the player
	 * 
	 * @param x spawn location
	 * @param y
	 * @param targets what to hit
	 * @throws SlickException
	 */
	default void createShot(float x, float y, ArrayList<Entity> targets) throws SlickException{
		// creates new laser
		try {
			Shot shot;
			ShotController.getShots().add(shot = new Shot(x, y, targets));
			// this kinda funky bit of code makes sure that something can fire 
			// even if there's nothing else on the screen
			if (targets.size() <=0) {
				FXController.createPlayerShotFx(shot);
			// if it's targeting the player, make it an enemy shot
			} else if (targets.get(0) instanceof Player) {
				FXController.createEnemyShotFx(shot);
			} else {
				FXController.createPlayerShotFx(shot);
			}
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
