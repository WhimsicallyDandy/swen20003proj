import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/**
 * Controls how powerups spawn. mostly through static methods
 * You know all these controller types could do an interface or inheritance thing
 * @author ngellie
 *
 */
public class PowerupController {
	
	private static final int TOTAL_POWERUP_TYPES = 2;
	// numerator of spawn chance
	private static final int SPAWN_CHANCE_I = 5;
	// denominator of spawn chance
	private static final int SPAWN_CHANCE_N = 100;
	
	// random number generator
	private static Random r = new Random();
	
	private static ArrayList<Powerup> pups;
	private static ArrayList<Powerup> toRemove;
	
	/** sets arraylists to new
	 * 
	 */
	public PowerupController() {
		pups = new ArrayList<>();
		toRemove = new ArrayList<>();
	}
	
	/** updates every powerup, removing as appropriate
	 * 
	 * @param input input keys from player
	 * @param delta millisecondsssssssssss passed each frame
	 */
	public void update(Input input, int delta) {
		// iterate through arraylist of powerups
		for (Powerup pu: pups) {
			pu.update(input, delta);
			// remove em if they're bad
			if (pu.getHealth() <= 0) {
				toRemove.add(pu);
			}
		}
		// cleanup
		for (Powerup p: toRemove) {
			pups.remove(p);
		}
		toRemove.clear();
	}
	
	/** renderer
	 * 
	 */
	public void render() {
		for (Powerup pu: pups) {
			pu.render();
		}
	}
	
	/* HELPER METHODS */
	/** has a random chance of spawning a random powerup
	 * 
	 * @param x location
	 * @param y location
	 */
	public static void maybeSpawnRandomPowerup(float x, float y) {
		// generate chance of spawning a powerup
		// if the senate determines that a powerup is to be spawned, do it. do it now Anakin!!!
		int temp = r.nextInt(SPAWN_CHANCE_N);
		if (temp < SPAWN_CHANCE_I) {
			spawnRandomPowerup(x, y);
		}
	}
	/** spawn a random kind of powerup
	 * 
	 * @param x location
	 * @param y location
	 */
	public static void spawnRandomPowerup(float x, float y) {
		int temp = r.nextInt(TOTAL_POWERUP_TYPES);
		spawnSpecPowerup(x, y, temp);
	}
	/** spawn a specific powerup
	 * 
	 * @param x location
	 * @param y
	 * @param i type
	 */
	public static void spawnSpecPowerup(float x, float y, int i) {
		try {
			pups.add(new Powerup(x, y, i));
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
}
