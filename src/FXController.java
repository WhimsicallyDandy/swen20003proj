
import java.util.ArrayList;

import org.newdawn.slick.SlickException;

import org.newdawn.slick.Graphics;

import org.newdawn.slick.Input;

/** Controls all the special fx on the screen
 */
public class FXController {
	
	/* file paths for the emitters */
	private static final String SHOT_FX_FILE = "res/shot_emitter.xml";
	private static final String BOOST_SHOT_FX_FILE = "res/boost_shot_emitter.xml";
	private static final String ENEMY_SHOT_FX_FILE = "res/en_shot_emitter.xml";
	private static final String THRUST_FILE = "res/test_emitter.xml";
	private static final String SHIELD_FILE = "res/shield_emitter.xml";
	private static final String BASE_DEATH_FILE = "res/base_death_emitter.xml";
	private static final String SPARK_FILE = "res/hit_spark.xml";
	/* images for the particles */
	private static final String TEST_PARTICLE = "res/test_particle.png";
	private static final String THRUST_IMG = "res/test_particle.png";
	private final static int THRUST_YOFFSET = 20;
	private final static int SHOT_YOFFSET = -6;
	
	// Slick controller for particles
	private static ArrayList<ParticleFx> systems;
	private static ArrayList<ParticleFx> toRemove;
	
	
	/**	Constructor
	 * 	iniatites some systems that are needed at the start
	 * 
	 * @param p player for locking on the thrust fx
	 * @throws SlickException
	 */
	public FXController(Player p) throws SlickException {
		
		systems = new ArrayList<>();
		toRemove = new ArrayList<>();

		// create player thrust fx
		systems.add(new ParticleFx(THRUST_IMG, THRUST_FILE, 0, 0, p, 0 ,THRUST_YOFFSET));
		
	}
	
	/**	Updates every particle effect system, destroying them if need be
	 * @param input takes player input
	 * @param delta milliseconds passed each frame
	 * @throws SlickException
	 */
	public void update(Input input, int delta) throws SlickException {
		// updates/removes particle systems
		for (ParticleFx sys : systems) {
			if (!sys.isOn()) {
				toRemove.add(sys);
			}
			sys.update(input, delta);	
		}
		// make sure once an fx is done, it is removed from memory	
		cleanArrayList(systems);
	}
	
	/** Render
	 * @param g Graphics for drawing all systems
	 */
	public void render(Graphics g) {
		for (ParticleFx sys: systems) {
			sys.render(g);
		}
	}
	
	/* HELPER METHODS */
	
	/** Template method for making a new fx
	 * 
	 * @param img particle image
	 * @param newFile file path for particle behaviour
	 * @param x spawn location
	 * @param y spawn location
	 */
	@SuppressWarnings("unused")
	private void createFx(String img, String newFile, float x, float y) {
		try {
			systems.add(new ParticleFx(img, newFile, x, y));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Create player shot fx at its location
	 * 
	 * @param shot a laser the player has fired
	 */
	public static void createPlayerShotFx(Shot shot) {
		// determines the correct effects depending on if the player has the fire rate powerup
		String temp = SHOT_FX_FILE;
		if (((Player)World.getPlayer()).isFireBoosted()) {
			temp = BOOST_SHOT_FX_FILE;
		} 			
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, temp, 0, 0, shot, 0, SHOT_YOFFSET));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**	Creates shield fx at target location
	 * 
	 * @param shield Shield object to lock to
	 */
	public static void createPlayerShieldFx(Shield shield) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, SHIELD_FILE, 0, 0, shield));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Creates particle system for enemy shot
	 * 
	 * @param shot laser enemy has fired. has fewer particles becase the boss fires a lot of them
	 */
	public static void createEnemyShotFx(Shot shot) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, ENEMY_SHOT_FX_FILE, 0, 0, shot, 0, -SHOT_YOFFSET));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** adds an enemy laser fx emitter to the system
	 * 
	 * @param shot enemy's laser
	 */
	public static void addEnemyShotFx(Shot shot) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, ENEMY_SHOT_FX_FILE, 0, 0, shot, 0, -SHOT_YOFFSET));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Creates a death effect at Entity's location for when it blows up
	 * 
	 * @param entity target, use locations
	 */
	public static void createDeathFx(Entity entity ) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, BASE_DEATH_FILE, entity.getX(), entity.getY()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Creates small sparking effect, like when a projectile hits something
	 * 
	 * @param x spawn location
	 * @param y spawn locaiton
	 */
	public static void createSparkFx(float x, float y) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, SPARK_FILE, x, y));
		} catch (Exception e) {
			e.printStackTrace();
		}
}
	public static void createDeathFx(float x, float y) {
		try {
			systems.add(new ParticleFx(TEST_PARTICLE, BASE_DEATH_FILE, x, y));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** Cleans an arraylist, removing items that match in toRemove
	 * 
	 * @param needsCleaning arraylist that has things to be deleted
	 */
	private void cleanArrayList(ArrayList<ParticleFx> needsCleaning) {
		for (ParticleFx temp : toRemove) {
			needsCleaning.remove(temp);
			//System.out.println("removed a system");
		}
		toRemove.clear();
	}
	
	
}
