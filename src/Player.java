import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/** hero of the day. the users way of interacting with the game
 * 	kill enemies and whatnot. rack up that score. i'm really tired
 *
 */
public class Player extends Entity implements Shooter {
	
	/* player-specific constants */
	public static final float PLAYER_SPEED = 0.5f;
	
	/* Initialise some importants */
	private final int PLAYER_INIT_HEALTH = 3;
	private final int PLAYER_BASE_DAMAGE_TAKEN = -1;
	private final int BASE_FIRE_RATE = 350;
	private final int BASE_POWERUP_DURATION = 5000;
	
	/* Initialise other entities */
	private ArrayList<Entity> enemies;
	private Shield playerShield;
	
	/* Image info */
	public static String PLAYER_SPRITE = "res/spaceship.png";
		
	/* powerup states*/ 
	private final int BOOST_FIRE_RATE = 150;
	private boolean powerupFireRate = false;
	private boolean powerupShield = false;
	
	private boolean shieldOn = false;
	
	// shields
	private static final int SHIELD_TIME = 3000;
	private int shieldDuration = SHIELD_TIME;
	private int shieldCount = 0;
	
	// rate of fire
	private int fireRate = BASE_FIRE_RATE;
	private int fireCount = fireRate;
	private int powerupFireCount = 0;
	
	/** creates a fresh new player ready to go
	 * @param x location
	 * @param y
	 * @param enemies all entities hostile to the player
	 * @throws SlickException
	 */
	public Player(float x, float y, ArrayList<Entity> enemies) throws SlickException {
		super(PLAYER_SPRITE, x, y, PLAYER_SPEED);
		this.enemies = enemies;
		playerShield = new Shield(getX(), getY(), enemies);
		setHealth(PLAYER_INIT_HEALTH);
	}
	
	/**	updates all input, location etc
	 * @param input get that input
	 * @param delta milliseconds each frame
	 * 
	 */
	public void update(Input input, int delta) {
		
		setInvincible(World.GAME_SPEED!=World.BASE_GAME_SPEED || shieldOn);
		// setting invincibility for testing and debugging
		if (World.DEBUG) {
			setInvincible(true);
		}
		// cheat out a fire rate boost for testing
		if (input.isKeyPressed(Input.KEY_F)) {
			powerupFireRate = true;
		}
		// powerups
		if (powerupFireRate) {
			if (powerupFireCount >= BASE_POWERUP_DURATION) {
				powerupFireRate = false;
				powerupFireCount = 0;
			} else {
				fireRate = BOOST_FIRE_RATE;
				powerupFireCount++;
			}
		} else {
			fireRate = BASE_FIRE_RATE;
		}
		// shield timing is already handled below in takedamage
		shieldDuration = (powerupShield)?BASE_POWERUP_DURATION:SHIELD_TIME;
		
		// debugging and testing
		if (input.isKeyPressed(Input.KEY_H)) {
			setHealth(10);
		}
		if (input.isKeyPressed(Input.KEY_P)) {
			createShield();
		}
		// moves player with WASD 
		movePlayer(input, delta);
		
		// creates new lasers at a fixed rate
		if (input.isKeyDown(Input.KEY_SPACE)) {
			if (fireCount >= fireRate) {
				try {
					// creates new shot
					createShot(getX(), getBox().getTop(), enemies);
				} catch (SlickException e) {
					// cool error catching stuff
					e.printStackTrace();
				}
				// resets counter
				fireCount=0;
			}	
		}
		fireCount++;
		// always ready to fire
		if (fireCount>fireRate) {
			fireCount = fireRate;
		}
		
		// adjusts hitbox position to current position
		super.update(input, delta);
		
		// collision with hostile entities		
		for (int i=0; i<enemies.size(); i++) {
			if (contactOther(enemies.get(i)) && !isInvincible()) {
				this.takeDamage(PLAYER_BASE_DAMAGE_TAKEN);
			}
		}  
		
		// handles shield updating
		if (shieldOn) {
			playerShield.setPos(getX(), getY());
			playerShield.update(input, delta);
			shieldCount++;
			// despawns shield
			if (shieldCount>=shieldDuration) {
				shieldCount = 0;
				playerShield.deactivate();
				shieldOn = false;
				shieldDuration = SHIELD_TIME;
				if (powerupShield) {
					shieldDuration = SHIELD_TIME;
				}
			}
		}
		
	}
	
	/** Renders player and maybe shield
	 * 
	 * @param g graphics for drawing and stuff. mostly used for printing when testing
	 */
	public void render(Graphics g) {
		super.render();
		if (shieldOn) {
			playerShield.render();
		}
	}
	
	/* HELPER METHODS */
	/**	moves player accoring to arrow keys
	 * 
	 * @param input get keyboard input
	 * @param delta milliseconds each frame
	 */
	private void movePlayer(Input input, int delta) {
		if (input.isKeyDown(Input.KEY_LEFT)) {
			xMoveLeft(getSpeed()*delta);
		}
		if (input.isKeyDown(Input.KEY_RIGHT))  {
			xMoveRight(getSpeed()*delta);
		}
		if (input.isKeyDown(Input.KEY_UP)) {
			yMoveUp(getSpeed()*delta);
		}
		if (input.isKeyDown(Input.KEY_DOWN)) {
			yMoveDown(getSpeed()*delta);
		}
	}
	
	// makes sure player stays within horizontal bounds
	public void xMoveLeft(float xNew) {
		if (!(getBox().getLeft()-getSpeed() < 0)) {
			addX(-xNew);
		}
	}
	public void xMoveRight(float xNew) {
		if (!(getBox().getRight()+getSpeed() > App.SCREEN_WIDTH)) {
			addX(xNew);
		}
	}
	// vertical movement, within bounds
	public void yMoveUp(float yNew) {
		if (!(getBox().getTop()-getSpeed() < 0)) {
			addY(-yNew);
		}
	}
	public void yMoveDown(float yNew) {
		if (!(getBox().getBottom()+getSpeed() > App.SCREEN_HEIGHT)) {
			addY(yNew);
		}
	}
	
	/** Damages the player
	 * 
	 * @param damage amount of health to lose
	 */
	public void takeDamage(int damage) {
		this.adjustHealth(damage);
		// if the player's health is at zero or lower, handle death
		if (this.getHealth()<=0) {
			// i think the in built slick exit is better than this
			this.deactivate();
			//setState(App.STATE_EXIT);
		}
		// otherwise protect the player with a shield
		createShield();
	}
	/** creates a new shield at target location
	 */
	private void createShield() {
		shieldOn = true;
		shieldCount = 0;
		//try {
			//playerShield = new Shield(getX(), getY(), enemies);
			playerShield.activate();
			FXController.createPlayerShieldFx(playerShield);
		//} catch (SlickException e) {
			//e.printStackTrace();
		//}
		// maybe some fancy effects
	}
	
	/** handles powerups depending on a randomly generated int
	 * 
	 * @param i flag for kind of powerup to use
	 */
	public void activatePowerup(int i) {
		if (i>=1) {
			// activate shield
			shieldDuration = BASE_POWERUP_DURATION;
			takeDamage(0);
			
		} else {
			// boost fire rate
			powerupFireRate = true;
			powerupFireCount = 0;
		}
	}
	
	/* Getters */
	
	// returns player shield
	public Shield getShield() {
		return playerShield;
	}
	// returns if firing boosted
	public boolean isFireBoosted() {
		return powerupFireRate;
	}
	
	/* Setters */
	// sets entities hostile to the player
	public void setHostiles(ArrayList<Entity> hostiles) {
		enemies = hostiles;
	}
	
	
}
