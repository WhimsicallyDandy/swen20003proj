import java.util.ArrayList;

import org.newdawn.slick.Input;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

/** Creates the world where all the game objects reside, known as Entities
 * 	contains controllers for handling multiple entities, spawns Entities
 */
public class World  {
	
	/* DEBUG */
	public static boolean DEBUG = false;
	
	/* State */
	private State worldState = State.ACTIVE;
	
	/* Initialise some mundane numbers (get it because they're not magic) */
	final float P_INIT_POSX = App.SCREEN_WIDTH/2f;
	final float P_INIT_POSY = App.SCREEN_HEIGHT/2f;
	public static final int MAX_BASIC_ENEMIES = 8;
	
	/* Initialise images */
	private static final String backgroundSpaceSprite = new String("res/space.png");
	
	/* Initialise background */
	Background background; 
	
	/* Initialise controllers */
	private static EnemyController enemyController;
	ShotController shotController;
	UIController uiController;
	PowerupController powerupController;
	FXController fxController;
	
	/* Initialise Entities */
	private Player player;
	private static ArrayList<Entity> hostiles = new ArrayList<>();
	private static ArrayList<Entity> friendlies = new ArrayList<>();
	
	/* game speed and time */
	public static int GAME_SPEED;
	public static final int BASE_GAME_SPEED = 1;
	public static final int WARP_GAME_SPEED = 5;
	private static int totalTime;
	
	/* DEBUGGING */
	double temp;
	double temp1;
	double temp2;
	double temp3;
	
	
	/** Big constructor boi. Look at the size of this lad. Absolute Unit 
	 */
	public World() throws SlickException {
		/* Initialise the game objects and world state */		
		
		shotController = new ShotController();
		
		enemyController = new EnemyController(EnemyController.WAVE_1, ShotController.getShots(), friendlies);		
		
		player = new Player(P_INIT_POSX, P_INIT_POSY, enemyController.getEnemies());
		
		uiController = new UIController(player);
		
		powerupController = new PowerupController();
		
		background = new Background(backgroundSpaceSprite);
		
		fxController = new FXController(player);
		
		setGameSpeed(BASE_GAME_SPEED);
		
		friendlies.add(player);
		
		totalTime = 0;
		
	}
	
	
	/** Updates all entities in the game
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	/* Some notes on specific input keys:
	 * left, right, up, down -> moves the player
	 * spacebar -> while held, fires lasers at a certain fire rate from the player
	 * S -> speeds up the game
	 * F -> give yourself the fire rate powerup
	 * P -> give yourself the shield powerup
	 * R -> restart the game, respawns the world
	 * H -> gives you a bunch of health
	 * Esc -> exits the game
	 * 
	 */
	public void update(Input input, int delta) throws SlickException {
		totalTime+=delta;
		// the hostiles ArrayList is anything that can damage the player
		hostiles = createHostiles(enemyController.getEnemies(), shotController.getEnemyShots());
		player.setHostiles(hostiles);
		// speed up for testing purposes or you want to see the background zoom by real fast
		if (input.isKeyDown(Input.KEY_S)) {
			setGameSpeed(WARP_GAME_SPEED);
		} else {
			setGameSpeed(BASE_GAME_SPEED);
		}
		// exit the game
		if (input.isKeyDown(Input.KEY_ESCAPE)) {
			System.exit(0);
		}
		delta *= GAME_SPEED;
		// controls game state
		if (player.isActive()) {
			// Update all of the entities in the game
			background.update(delta);
			enemyController.update(input, delta);
			player.update(input, delta);
			shotController.update(input, delta);
			powerupController.update(input, delta);
			fxController.update(input, delta);
			
		} else if (!player.isActive()) {
			// exit the game
			worldState = State.INACTIVE;
		}
		
	}
	
	/** Renders all Entities
	 * @param g Slick's graphics handler, used to draw strings, shapes and other misc things
	 */
	public void render(Graphics g) {
		// Draw all of the entities in the game, in this order
		background.renderTiles();
		player.render(g);
		
		enemyController.render();
		shotController.render();
		uiController.render(g);
		powerupController.render();
		fxController.render(g);
	
	}
	
	/* HELPER METHODS */
	private ArrayList<Entity> createHostiles(ArrayList<Entity> a, ArrayList<Entity> b) {
		ArrayList<Entity> temp = new ArrayList<>();
		temp.addAll(a);
		temp.addAll(b);
		return temp;
	}
	
	/* Getters */
	// returns enemyController
	public EnemyController getEnemyController() {
		return enemyController;
	}
	// get current World State
	public State getState() {
		return worldState;
	}
	// get World Speed
	public static int getGameSpeed() {
		return GAME_SPEED;
	}
	// get World time passed
	public static int getTotalTime() {
		return totalTime;
	}
	// get all hostiles to player
	public static ArrayList<Entity> getPlayerHostiles() {
		return hostiles;
	}
	// get all friendlies to the player
	public static ArrayList<Entity> getPlayerFriendlies() {
		return friendlies;
	}
	// get the player
	public static Entity getPlayer() {
		if (friendlies.size()>0) {
			return friendlies.get(0);
		} else return null;
	}
	// get the boss
	public static BasicBoss getBoss() {
		for (Entity entity: enemyController.getEnemies()) {
			if (entity instanceof BasicBoss) {
				return (BasicBoss)entity;
			}
		}
		
		return null;
	}
	
	/* Setters */
	public static void setGameSpeed(int s) {
		GAME_SPEED = s;
	}
}

