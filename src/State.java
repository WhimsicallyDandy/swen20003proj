/* code paraphrased from: https://stackoverflow.com/questions/8811815/is-it-possible-to-assign-numeric-value-to-an-enum-in-java */

/** Just a simple state handling class
 */
public enum State {
	INACTIVE, ACTIVE;
}
