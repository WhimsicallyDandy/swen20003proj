import java.util.ArrayList;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/** protects an entity from damage, destorys things on contact
 */
public class Shield extends Entity {
	// img path
	private static String shieldSprite = "res/shield.png";
	// hostiles
	private ArrayList<Entity> enemies;
	
	/** create a new shield to be activated and defend
	 * 
	 * @param x location
	 * @param y location
	 * @param enemies what to potentially destroy
	 * @throws SlickException
	 */
	public Shield(float x, float y, ArrayList<Entity> enemies) throws SlickException {
		super(shieldSprite, x, y, 0);
		this.enemies = enemies;
		// so it is only active when specifically called
		this.deactivate();
	}
	/** keeps the sheild in the correct place and collision with other entities
	 * 
	 */
	public void update(Input input, int delta) {
		super.update(input, delta);
		if (this.isActive()) {
			enemies = World.getPlayerHostiles();
			// handles collision with other entities
			// it's less of a challenge just being able to ram enemies while invincible
			// but it's lots of fun and shows off the particle effects i put too much time into
			for (Entity enemy: enemies) {
				// but doesn't instakill the boss because that's too much cheating
				if (this.contactOther(enemy) && !(enemy instanceof BasicBoss)) {
					// sets for deletion, need destruction handling state
					enemy.setHealth(0);
				}
			}
		}
	}
}
