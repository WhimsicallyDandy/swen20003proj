import java.util.ArrayList;

import org.newdawn.slick.Input;


public class ShotController {
	
	public static int TOTAL_SHOTS = 0;
	private static ArrayList<Shot> activeShots;
	private ArrayList<Shot> toRemove;
	
	public ShotController() {
		activeShots = new ArrayList<>();
		toRemove = new ArrayList<>(); 
	}
	
	/** Updates all shots destroying as needed
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {
		// iterate through shots and destroy as needed if offscreen
		for (Shot shot : activeShots) {
			if (shot.getY()>App.SCREEN_HEIGHT || shot.getY()<0 || shot.getHealth()<=0) {
				toRemove.add(shot);
				shot.deactivate();
			}
			shot.update(input, delta);	
		}
		// cleanup
		for (Shot temp: toRemove) {
			activeShots.remove(temp);
			TOTAL_SHOTS--;
		}
		toRemove.clear();
	}
	
	/** render
	 * 
	 */
	public void render() {
		for (Shot shot: activeShots) {
			shot.render();
		}
	}
	
	/* Getters */
	public static ArrayList<Shot> getShots() {
		return activeShots;
	}
	
	/** returns an arraylist of all shots that target the player
	 * 
	 * @return arraylist of projectiles as above
	 */
	public ArrayList<Entity> getEnemyShots() {
		ArrayList<Entity> temp = new ArrayList<>();
		for (Shot shot: activeShots) {
			if (shot.getTarget() instanceof Player) {
				temp.add(shot);
			}
		}
		return temp;
	}
}
