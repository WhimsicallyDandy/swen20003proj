import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

import java.lang.Math;
/** Enemy that waggles down in a sine pattern
 */
public class SineEnemy extends Enemy {
	
	private static final String SINE_ENEMY_SPRITE = "res/sine-enemy.png";
	private static final float SINE_ENEMY_SPEED = 0.15f; 
	private static final int AMP_CONST = 96;
	private static final int PERIOD_CONST = 1500;
	private static final int SINE_ENEMY_SCORE = 100;
	
	private final float X_AXIS;
	
	
	/** initialises the position and axis for wiggle
	 * 
	 * @param x horiz spawn
	 * @param y vert spawn
	 * @param activateTime timer until ready to go
	 * @throws SlickException
	 */
	public SineEnemy(float x, float y, int activateTime) throws SlickException {
		super(SINE_ENEMY_SPRITE, x, y, SINE_ENEMY_SPEED, activateTime, SINE_ENEMY_SCORE);
		X_AXIS = x;
	}
	
	/** Updates the wiggly boi
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {
		super.update(input, delta);
	}
	
	/* HELPER FUNCTIONS */
	
	/** this enemy's active state
	 * @param delta milliseconds each frame
	 */
	public void activeUpdate(int delta) {
		super.activeUpdate(delta);
		// calculates a sin pattern using math and the initial x spawn as the axis
		this.setX(X_AXIS+sinOffset());
	}
	
	// calculates sine movement
	private float sinOffset() {
		return (float)(AMP_CONST*Math.sin(2*Math.PI/PERIOD_CONST*(World.getTotalTime()-getActivateTime())));
	}
	
}
