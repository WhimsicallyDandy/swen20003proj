import java.util.Random;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/** The big bad boss of the game, multifiring on all cylinders
 * 	unique behaviour, large health, and a health bar
 */
public class BasicBoss extends Enemy implements Shooter {
	
	/* Hey lets check this thing is actually out there */
	public static boolean BOSS_SPAWNED = false;
	
	/* CONSTANTS */
	private final static String BOSS_SPRITE = "res/boss.png";
	private final static float X_SPEED = 0.2f;
	private final float ENTER_SPEED = 0.05f;
	private final float ENTER_YDEST = 72f;
	private final float FIRING_MOVE_MODIFIER_BASE = 0.5f;
	
	private final float FIRE_OFFSET_WIDE = 97;
	private final float FIRE_OFFSET_NARROW = 74;
	private final int FIRE_RATE = 200;
	//private final int FIRE_DURATION = 3000; used for testing purposes
	private final int WAIT_DURATION_LOOP_START = 5000;
	private final int WAIT_DURATION_LOOP_MID = 2000;
	public static final int BASIC_BOSS_HEALTH = 60;
	private static final int BOSS_SCORE = 5000;
	private final int LBOUND = 128;
	private final int RBOUND = App.SCREEN_WIDTH-LBOUND;
	
	// States, experimenting with Enums
	private enum BossState {
		ENTERING, WAITING, MOVING, FIRE_MOVE;
	}
	BossState state;
	
	// all stuff for firing shots
	private int waitTime;
	private int waitCount = 0;
	private int fireCount = FIRE_RATE;
	
	private float xDest;
	// contains spawn locations for the boss projectiles
	private float[] shotSpawnX = {
			-FIRE_OFFSET_WIDE, 
			-FIRE_OFFSET_NARROW, 
			FIRE_OFFSET_NARROW, 
			FIRE_OFFSET_WIDE};
	
	// is it firing, is it ready to fire after moving?
	private boolean firing = false;
	private boolean readyToFire = false;
	
	// random gen for choosing locations
	private Random r = new Random();
	
	/** Constructor
	 * @param x horizontal spawn location
	 * @param y vertical spawn location
	 * @param activateTime how far into the game it becomes active
	 * @throws SlickException Slick Exception handling
	 */
	public BasicBoss(float x, float y, int activateTime) throws SlickException {
		super(BOSS_SPRITE, x, y, X_SPEED, activateTime, BOSS_SCORE);
		setHealth(BASIC_BOSS_HEALTH);
		state = BossState.ENTERING;
	}
	
	/** Updates the boss object, and loops through its behaviour cycle
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {
		super.update(input, delta);
	}
	
	/** the boss' active state
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void activeUpdate(int delta) {
		setRender(true);
		BOSS_SPAWNED = true;
		switch (state) {
		/* case manager for the boss */
		// Handles when the boss enters, dramatically loom and shift downward until a point
		case ENTERING:
			if (getY()>=ENTER_YDEST) {
				waitTime = WAIT_DURATION_LOOP_START;
				xDest = r.nextInt(RBOUND-LBOUND)+LBOUND;
				state = BossState.WAITING;
			} else {
				addY(ENTER_SPEED);
			}
			break;
		// Moves the boss towards a random bounded x, when it is not firing
		case MOVING:
			waitTime =  WAIT_DURATION_LOOP_MID;
			if ((int)getX() != (int)xDest) {
				addX(Math.signum(xDest-getX())*X_SPEED);
			} else {
				readyToFire = true;
				state = BossState.WAITING;
			}
			break;
		// Waits for a certain amount of time. and moves to a location, firing if it moved without firing previously
		case WAITING:
			waitCount++;
			if (waitCount>=waitTime) {
				xDest=r.nextInt(RBOUND-LBOUND)+LBOUND;
				state=BossState.MOVING;
				if (readyToFire) {
					waitTime = WAIT_DURATION_LOOP_MID;
					state=BossState.FIRE_MOVE;
				}
				waitCount = 0;
			}
			break;
		// Move while firing, and when a destination is reached, wait a period of time and continue firing
		case FIRE_MOVE:
			firing = true;
			if ((int)getX() != (int)xDest) {
				// moves the boss in the correct direction
				addX(Math.signum(xDest-getX())*X_SPEED*FIRING_MOVE_MODIFIER_BASE);
			} else {
				if (waitCount>=waitTime) {
					firing = false;
					waitTime = WAIT_DURATION_LOOP_START;
					waitCount = 0;
					readyToFire = false;
					state = BossState.WAITING;
				} else {
					waitCount++;
				}
			}
			break;
		}
		
		// fires projectiles, iterating through the array of spawn locations
		if (firing) {
			if (fireCount >= FIRE_RATE) {
				for (float i: shotSpawnX) {
					try {
						createShot(getX()+i, getY()+(getImage().getHeight()/2), World.getPlayerFriendlies());
					} catch (SlickException e) {
						e.printStackTrace();
					}
				}
				fireCount = 0;
			}
			fireCount++;			
		}
	}	
	
	/* HELPER METHODS */
	// returns the boss' max health
	public int getMaxHealth() {
		return BASIC_BOSS_HEALTH;
	}
}
