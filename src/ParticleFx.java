import java.io.File;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;

/**
 * a bucket containing a particle system, location, and emitters
 *
 */
public class ParticleFx {
	/* initiate all Classes needed */
	// Slick controller for particles
	private ParticleSystem system;
	private ConfigurableEmitter emitter;

	private boolean on = true;

	// max particles allowed in a system
	private final int MAX_PARTICLES = 500;
	// max time allowed for an emitter to stay alive if it is not linked to an entity
	private final int TIME_THRESHOLD = 1000;

	private int count = 0;

	String imgname;
	String filename;

	private Entity entity = null;
	private boolean hasEntity;

	private float xOffset = 0;
	private float yOffset = 0;

	
	/**
	 * @param imgname img path
	 * @param filename file path
	 * @param x location
	 * @param y location
	 * @param entity for linking to an entity location
	 * @param xOffset
	 * @param yOffset positions relative to the entity its linked to
	 * @throws SlickException
	 */
	public ParticleFx(String imgname, String filename, float x, float y) throws SlickException {
		this.imgname = imgname;
		this.filename = filename;
		this.init(x, y);
		hasEntity = false;
	}
	public ParticleFx(String imgname, String filename, float x, float y, Entity entity) throws SlickException {
		this.imgname = imgname;
		this.filename = filename;
		this.entity = entity;
		this.init(x, y);
		hasEntity = true;
	}
	public ParticleFx(String imgname, String filename, float x, float y, Entity entity, float xOffset, float yOffset)
			throws SlickException {
		this.imgname = imgname;
		this.filename = filename;
		this.entity = entity;
		hasEntity = true;
		this.init(x, y);
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	/** Updates all emitters
	 * 
	 * @param input takes user input
	 * @param delta milliseconds each frame
	 * @throws SlickException
	 */
	public void update(Input input, int delta) throws SlickException {
		// updates particle system
		system.update(delta);
		// if it is linked to an entity, update its position
		// make sure it's within bounds too and active
		if (hasEntity) {
			if (entity.isActive() && entity.getHealth() > 0 && !(this.getY() < 0)
					&& !(this.getY() >= App.SCREEN_HEIGHT + yOffset)) {
				// set position to current entity
				this.setPos(entity.getX() + xOffset, entity.getY() + yOffset);

			} else {
				// deactivates emitter
				on = false;
			}
		} else {
			// if time's up, end it
			if (count >= TIME_THRESHOLD) {
				on = false;
			}
			count++;
		}
		// respawns
		if (input.isKeyPressed(Input.KEY_K)) {
			// this.init(this.getX(), this.getY());
			this.init(0, 0);
		}
	}

	/**	render
	 * 
	 * @param g for drawing cool things in slick
	 */
	public void render(Graphics g) {
		system.render();
	}

	/* HELPER METHODS */

	/** Initialises fx machine
	 * 
	 * @param x locations
	 * @param y
	 * @throws SlickException
	 */
	public void init(float x, float y) throws SlickException {
		try {
			// assigns the emitter a file and an image
			system = new ParticleSystem(new Image(imgname), MAX_PARTICLES);
			// adds the emitter to the system
			emitter = ParticleIO.loadEmitter(new File(filename));
			emitter.setPosition(0, 0);
			this.setPos(x, y);
			// adds emitter to the system
			system.addEmitter(emitter);

		} catch (Exception e) {
			e.printStackTrace();
		}
		system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
	}

	public void setPos(float x, float y) {
		system.setPosition(x, y);
	}

	public float getX() {
		return system.getPositionX();
	}

	public float getY() {
		return system.getPositionY();
	}

	public boolean isOn() {
		return on;
	}

}
