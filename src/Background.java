import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/** Draws a tiled background
 * Handles info separately from Entities so as not to interfere
 * 
 */
public class Background {
	private static final float SCROLL_SPEED = 0.2f;
	
	private Image tile;
	private float x = 0f;
	private float y = 0f;
	private float displayWidth = App.SCREEN_WIDTH;
	private float displayHeight = App.SCREEN_HEIGHT;
	
	
	/** Constructor
	 * @param sprite the path for the background image to be tiled 
	 */
	public Background (String sprite) throws SlickException {
		this.tile = new Image(sprite);
	}
	
	/** Update method:adjusts the position of the background
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(int delta) {
		// loops y through the dimensions of the image
		y += SCROLL_SPEED*delta;
		if (y > tile.getHeight()) {
			y-=tile.getHeight();
		}
	}
	
	/*
	 * RENDERER
	 */
	// this is the base render, render tiles is used to fill the screen
	public void render() {
		tile.draw(x, y);
	}
	/* renders all tiles to fill up the screen */
	public void renderTiles() {
		// fill horizontally
		for (float left=x; left<displayWidth; left+=tile.getWidth()) {
			// fill vertically. subtracting the display height to fill the top of the screen
			for (float top=y-displayHeight; top<displayHeight; top+=tile.getHeight()) {
				tile.draw(left, top);
			}
		}
	}
	
	/* Getters */
	// gets tile
	public Image getTile() {
		return tile;
	}
	// gets scroll speed
	public float getSpeed() {
		return SCROLL_SPEED;
	}
	
	/* Setters*/
	// sets new tile
	public void setTile(Image newTile) {
		this.tile = newTile.copy();
	}
}
