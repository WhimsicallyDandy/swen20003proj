
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Image;

import org.newdawn.slick.Input;

/** Handles all enemy movement, destruction, and spawning
 * @param file a text file containing info for sets of enemies, with data
 *        separated by commas
 */
public class EnemyController {

	// char that signifies a comment in the file read
	final static char COM_CHAR = '#';
	// I was ambitious and thought I could add more waves in the future
	public final static String WAVE_1 = "res/waves.txt";

	/* this chap loves their ArrayLists */
	// active enemies
	private ArrayList<Entity> inactiveEnemies = new ArrayList<>();
	// dormant enemies, kept just above the screen
	private ArrayList<Entity> activeEnemies = new ArrayList<>();
	private ArrayList<Entity> toRemove = new ArrayList<>();
	private ArrayList<Shot> shots = new ArrayList<>();
	private ArrayList<Entity> friendlies;

	/**	Reads through a file and spawns enemies as appropriate
	 * 
	 * @param file file to be read
	 * @param shots shots to add enemy shots to
	 * @param friendlies targets
	 * @throws SlickException slick y
	 */
	public EnemyController(String file, ArrayList<Shot> shots, ArrayList<Entity> friendlies) throws SlickException {
		
		// instance vars for reading data
		Scanner sc;
		String line;
		String[] data;
		try {
			sc = new Scanner(new File(file));
			// reads through each line, ignoring if it's a comment
			while (sc.hasNextLine()) {
				if ((line = sc.nextLine()).charAt(0) == COM_CHAR) {
					continue;
				}
				// adding data to an enemy is reliant on the file having a specific data format
				// 0=type, 1=x spawn position, 2=timer until active
				data = line.split(",");
				// creates an enemy depending on the type
				createInactiveEnemy(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.friendlies = friendlies;
	}

	/** Updates all enemies. Activates and Destroys them
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {

		// iterate through the inactive enemies, loading them into the game when needed
		for (Entity enemy : inactiveEnemies) {

			if (enemy.getState().equals(State.ACTIVE)) {
				activeEnemies.add(enemy);
				toRemove.add(enemy);
				enemy.setY(-enemy.getBox().getHeight() / 2);
			}
			enemy.update(input, delta);
		}
		cleanArrayList(inactiveEnemies);

		// iterate through active enemies, removing them when destroyed
		for (Entity enemy : activeEnemies) {

			// handle offscreen removal (no scores or powerups)
			if (enemy.getY() - (enemy.getImage().getHeight() / 2) > App.SCREEN_HEIGHT) {

				toRemove.add(enemy);
			}
			// destroy the enemy, handle powerups and scores
			if (enemy.getHealth() <= 0) {

				toRemove.add(enemy);
				// sets the bossspawned val as false when it dies
				if (enemy instanceof BasicBoss) {
					BasicBoss.BOSS_SPAWNED = false;
				}
				// a small chance of spawning a powerup on each enemy death
				PowerupController.maybeSpawnRandomPowerup(enemy.getX(), enemy.getY());
				// add score to the ui
				UIController.addScore(((Enemy) enemy).getScore());
				// create a death effect
				FXController.createDeathFx(enemy);
			}
			enemy.update(input, delta);
		}
		cleanArrayList(activeEnemies);
	}

	/**
	 * draw em all
	 */
	public void render() {
		for (Entity enemy : activeEnemies) {
			enemy.render();
		}
	}

	/* HELPER METHODS */
	// clean up an ArrayList with things that need to be removed
	private void cleanArrayList(ArrayList<Entity> needsCleaning) {
		for (Entity temp : toRemove) {
			needsCleaning.remove(temp);
		}
		toRemove.clear();
	}

	// creates a new enemy
	private void createInactiveEnemy(String[] array) throws SlickException {
		inactiveEnemies.add(generateEnemy(array));
	}
	
	private Enemy generateEnemy(String[] array) throws SlickException {
		switch (array[0]) {
		// create basic enemy
		case "BasicEnemy":
			return new Enemy(Enemy.BASE_ENEMY_SPRITE, 
					Float.parseFloat(array[1]),
					-(new Image(Enemy.BASE_ENEMY_SPRITE).getHeight()) / 2,
					Enemy.BASE_ENEMY_SPEED,
					Integer.parseInt(array[2]));
		// create Sine Enemy
		case "SineEnemy":
			return new SineEnemy(Float.parseFloat(array[1]), 0f, Integer.parseInt(array[2]));
		// create Shooter Enemy
		case "BasicShooter":
			return new BasicShooterEnemy(Float.parseFloat(array[1]), 0f, Integer.parseInt(array[2]), shots, friendlies);
		// create a bosssss
		case "Boss":
			return new BasicBoss(Float.parseFloat(array[1]), 0f, Integer.parseInt(array[2]));
		// if none of these can been created, spawn a basic enemy with a placeholder sprite
		default:
			return new Enemy(Player.PLAYER_SPRITE, Float.parseFloat(array[1]), 0f, Enemy.BASE_ENEMY_SPEED,
					Integer.parseInt(array[2]));
		}
	}

	/* Getters */
	// gets current number of inactive enemies
	public int getInactiveNum() {
		return inactiveEnemies.size();
	}
	// gets current number of active enemies
	public int getActiveNum() {
		return activeEnemies.size();
	}
	// returns the ArrayList of enemies
	public ArrayList<Entity> getEnemies() {
		return activeEnemies;
	}
	// get a specific enemy
	public Entity getEnemy(int i) {
		return activeEnemies.get(i);
	}
	// get the boss. returns null if the boss is not spawned
	public Entity getBoss() {
		Entity check = null;
		for (Entity c : activeEnemies) {
			if (c instanceof BasicBoss) {
				check = c;
				break;
			}
		}
		return check;
	}

	/* setters */
}
