import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/** aw yeah its the powerups boiiiiiiiiiiiis
 * 
 * 	drifts down the screen, on contact with the player, gives them a powerup
 */
public class Powerup extends Entity {
	
	// speed to move down the screen
	private static float POWERUP_DRIFT_SPEED = 0.1f;
	private static String SHIELD_P_IMG = "res/shield-powerup.png";
	private static String SHOTSPEED_P_IMG = "res/shotspeed-powerup.png";
	
	// determines what powerup the player gets on contact
	private int type;
	
	/** Creates new powerup at location
	 * 
	 * @param x location
	 * @param y locale
	 * @param type flavour of boost
	 * @throws SlickException
	 */
	public Powerup(float x, float y, int type) throws SlickException {
		super(SHIELD_P_IMG, x, y, POWERUP_DRIFT_SPEED);
		this.type = type;
		// sets sprite as appropriate
		// could be extended to an array of powerups
		if (type>=1) {
			setSprite(SHIELD_P_IMG);
		} else setSprite(SHOTSPEED_P_IMG);
	}
	
	/**floats down the screen
	 *  if contacts the player, destroy and activate effect
	 */
	public void update(Input input, int delta) {
		// move down the screen at speed
		super.update(input, delta);
		addY(getSpeed());
		// if contacts player, activate powerup
		if (contactOther(World.getPlayer())) {
			activatePowerup(type);
			this.setHealth(0);
		}
	}
	
	/* HELPER METHODS */
	/** activates effect on player
	 * 
	 * @param i random int type
	 */
	public void activatePowerup(int i) {
		this.setHealth(0);
		((Player)World.getPlayer()).activatePowerup(i);
	}
}
