import java.util.ArrayList;
import java.lang.Math;

import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/* thanks Eleanor for the rad code */
import utilities.BoundingBox;

/** Blueprint for almost everything in the game
 * 	can have collision and health
 */
public class Entity {
	
	/* all entities may have the following */
	private Position pos;
	private Image sprite;
	private float speed;
	private boolean renderOn = true;
	private int health = 1;
	private boolean invincible = false;
	
	/* optionally they may also have */
	private ArrayList<Entity> targets;
	
	/* states are also a thing */
	private State state = State.ACTIVE;
	/* bounding box/hitbox/collision detection */
	private BoundingBox hitbox;
	
	/**	Creates a new entity at a specific location
	 * @param imageSrc filepath for sprite
	 * @param x spawn
	 * @param yspawn
	 * @param speed the rate at which it locomotes through virtual space
	 * @throws SlickException
	 */
	public Entity(String imageSrc, float x, float y, float speed) throws SlickException {
		// Initialises image source and location, with overflow for a sprite
		this.sprite = new Image(imageSrc);
		pos = new Position(x, y);
		this.speed = speed;
		hitbox = new BoundingBox(x, y, sprite.getWidth(), sprite.getHeight());
	}
	
	/**	Updates the collision information depending on position
	 * @param input gets keyboard presses and mouse clicks
	 * @param delta so many milliseconds (see App for a serious description)
	 * 			no wait it's the number of milliseconds that have passed between
	 * 			each update frame please don't deduct marks
	 */
	public void update(Input input, int delta) {
		// Adjust hitbox location
		hitbox.setCoords(getX(), getY());
	}
	
	/**	Renders the entity on the screen as long as its ok to
	 */
	public void render() {
		if (renderOn) {
			sprite.draw(getBox().getLeft(), getBox().getTop());
		}
		
	}
	
	/* HELPER METHODS */
	
	// returns true when something makes contact with something else
	public Boolean contactOther(Entity other) {
		return (this.hitbox.intersects(other.hitbox));
	}
			
	/* Big Getter Bois */
	// x getter
	public float getX() {
		return pos.getX();
	}
	// y getter
	public float getY() {
		return pos.getY();
	}
	// sprite getter
	public Image getSprite() {
		return this.sprite;
	}
	// speed getter
	public float getSpeed() {
		return this.speed;
	}
	// health getter
	public int getHealth() {
		return health;
	}
	// image getter
	public Image getImage() {
		return sprite;
	}
	// bounding box getter
	public BoundingBox getBox () {
		return hitbox;
	}
	// state getter
	public State getState() {
		return state;
	}
	public boolean isActive() {
		return (state.equals(State.ACTIVE));
	}
	// invincibility getter
	public boolean isInvincible() {
		return invincible;
	}
	// target getter
	public ArrayList<Entity> getTargets() {
		return targets;
	}
	
		
	/* Big Setter Bois */
	// regular x and y setters in case an entity needs to be teleported
	public void setX(float xNew) {
		pos.setX(xNew);
	}
	public void setY(float yNew) {
		pos.setY(yNew);
	}
	public void setPos(float x, float y) {
		pos.setPos(x, y);
	}
	public void addX(float x) {
		pos.setX(getX()+x);
	}
	public void addY(float y) {
		pos.setY(getY()+y);
	}
	/**	move in a direct line to a location
	 * 	doesn't work atm but i tried
	 * 
	 * @param x horizontal destination
	 * @param y vertical destination
	 */
	public void moveTo(float x, float y) {
		// currently uses base speed, maybe implement sin/cosine
		
		addX(clampedSpeed(x)*Math.signum(x-getX()));
		addY(clampedSpeed(y)*Math.signum(y+getY()));
	}
	/**
	 * Makes sure the entity moves to the exact location
	 * @param x any part of a coordinate destination
	 * @return the minimum of the speed or the difference
	 * 			b/w current location and destination on one plane
	 */
	private float clampedSpeed(float x) {
		float dist = x-getX();
		return (dist<speed)?dist:speed;
	}
	// set speed
	public void setSpeed(float newSpeed) {
		speed = newSpeed;
	}
	// set health to value
	public void setHealth(int health) {
		this.health=health; 
	}
	// take damage
	public void adjustHealth(int health) {
		this.health+=health;
	}
	// sets a new image
	public void setSprite(Image newSprite) {
		this.sprite = newSprite.copy();
	}
	public void setSprite(String filename) throws SlickException {
		this.sprite = new Image(filename);
	}
	// set render state
	public void setRender(boolean option) {
		renderOn = option;
	}
	// set activation
	public void activate() {
		state = State.ACTIVE;
	}
	public void deactivate() {
		state = State.INACTIVE;
	}
	// set invincibility
	public void setInvincible(boolean i) {
		invincible = i;
	}
	// set targets
	public void setTargets(ArrayList<Entity> newTargets) {
		this.targets = newTargets;
	}
}
