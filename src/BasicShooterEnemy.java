import java.util.ArrayList;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;
import java.util.Random;


/**
 * working, NOW NEED TO IMPLEMENT SHOOTING FUNCTIONALITY
 * @author ngellie
 *
 */
public class BasicShooterEnemy extends Enemy implements Shooter {
	
	// initialise everything common to the class
	private static final String BASIC_SHOOTER_ENEMY_SPRITE = "res/basic-shooter.png";
	private static final float BASIC_SHOOTER_ENEMY_SPEED = 0.2f;
	
	private static final int HI_BOUND = 464;
	private static final int LO_BOUND = 48;
	private static final int SHOOTER_ENEMY_SCORE = 200;
	
	// shot info
	private final int BASE_FIRE_RATE = 3500;
	
	private int fireRate;
	private int fireCount;
	private ArrayList<Entity> playerFriendlies;
	
	// random y destination between bounds
	Random rand = new Random();
	private final int yDest = rand.nextInt(HI_BOUND-LO_BOUND)+LO_BOUND;
	
	// movement and entry
	boolean entering = true;
	
	public static int temp = 0;
	public static Entity temp2;
	
	/**
	 * 
	 * @param x horiz spawn
	 * @param y vert spawn
	 * @param activateTime timer until ready to go
	 * @param shots arraylist of shots
	 * @param playerFriendlies targets 
	 * @throws SlickException thank you Slick for your exceptions
	 */
	public BasicShooterEnemy(float x, float y, int activateTime, ArrayList<Shot> shots, ArrayList<Entity> playerFriendlies) throws SlickException {
		super(BASIC_SHOOTER_ENEMY_SPRITE, x, y, BASIC_SHOOTER_ENEMY_SPEED, activateTime, SHOOTER_ENEMY_SCORE);
		fireRate = BASE_FIRE_RATE;
		fireCount = fireRate;
		this.playerFriendlies = World.getPlayerFriendlies();
	}
	
	/** Updates the shooter boi
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {
		playerFriendlies = World.getPlayerFriendlies();
		// does not shoot until it reaches a random y destination
		if (getY() >= yDest) {
			entering = false;
		}
		// fires its lazor
		if (!entering) {
			if (fireCount >= fireRate) {
				try {
					// shoots a projectile that targets the player
					createShot(getX(), getY()+(getImage().getHeight()/2), playerFriendlies);
				} catch (SlickException e) {
					e.printStackTrace();
				}
				fireCount=0;
			}
		}
		super.update(input, delta);
		fireCount++;
	}
	
	/**
	 * @param delta as above
	 */
	public void activeUpdate(int delta) {
		// enter the screen, and once a destination is reached, start firing
		setRender(true);
		if (entering) {
			// moves down the screen
			this.addY(BASIC_SHOOTER_ENEMY_SPEED*delta);
		}
	}
}
