package fx;

import java.io.File;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.particles.ConfigurableEmitter;
import org.newdawn.slick.particles.ParticleIO;
import org.newdawn.slick.particles.ParticleSystem;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.GameContainer;

/*
 * Need multiple emitters for different thrust states
 */
public class ParticleTest {
	private ParticleSystem system;
	private ConfigurableEmitter emitter;
	
	public void init() throws SlickException {
		try {
			Image image = new Image("res/test_particle.png");
			system = new ParticleSystem(image, 500);
			
			File thrustConfig = new File("files/test_emitter.xml");
			emitter=ParticleIO.loadEmitter(thrustConfig);	
			emitter.setPosition(0, 20);
			system.addEmitter(emitter);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
	}
	
	public void update(Input input, int delta) throws SlickException {
		system.update(delta);
		
		if (input.isKeyPressed(Input.KEY_K)) {
			try {
				Image image = new Image("res/test_particle.png");
				system = new ParticleSystem(image, 500);
				
				File thrustConfig = new File("files/test_emitter.xml");
				emitter=ParticleIO.loadEmitter(thrustConfig);	
				emitter.setPosition(0, 20);
				system.addEmitter(emitter);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			system.setBlendingMode(ParticleSystem.BLEND_ADDITIVE);
		}
	}
	
	public void render(Graphics g) {
		g.drawString("Test Particle | Particle count: " + system.getParticleCount(), 10, 25);
		system.render();
	}
	
	public void setPos(float x, float y) {
		system.setPosition(x, y);
	}
	
	public float getX() {
		return system.getPositionX();
	}
	public float getY() {
		return system.getPositionY();
	}
}
