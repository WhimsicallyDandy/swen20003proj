import java.lang.Math;

/** Handles all positioning for entities
 */
public class Position {
	private float x;
	private float y;
	
	// creates new position
	public Position (float x, float y) {
		this.x = x;
		this.y = y;
	}
	// copies other position
	public Position(Position other) {
		this.x = other.getX();
		this.y = other.getY();
	}
	// calculate distance from another Position
	public float getDist(Position other) {
		return (float)Math.sqrt((Math.pow(x-other.getX(), 2))+(Math.pow(y-other.getY(), 2)));
	}
	// calculate distance from another location
	public float getDist(float ox, float oy) {
		return (float)Math.sqrt((Math.pow(x-ox, 2))+(Math.pow(y-oy, 2)));
	}
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	public void setX(float x) {
		this.x = x;
	}
	public void setY(float y) {
		this.y = y;
	}
	public void addX(float x) {
		this.x += x;
	}
	public void addY(float y) {
		this.y += y;
	}
	public void setPos(float x, float y) {
		this.x = x;
		this.y = y;
	}
}
