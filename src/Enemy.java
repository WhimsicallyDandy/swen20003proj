import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

/** The basic Enemy class in the game
 * 	just a small thing that floats down the screen
 * 	a trivial obstacle (until there's lots of them)
 */
public class Enemy extends Entity {
	
	private static final int BASE_ENEMY_SCORE = 50;
	private static final int BASE_ENEMY_DAMAGE = 1;
	
	public final static float BASE_ENEMY_SPEED = 0.2f;
	public final static String BASE_ENEMY_SPRITE = "res/basic-enemy.png";
	
	
	@SuppressWarnings("unused")
	// total enemies that have been spawned
	private static int enemyCount = 0;
	
	private int activateTime;
	private int activateCount = 0;
	private int score = BASE_ENEMY_SCORE;
	private int damage = BASE_ENEMY_DAMAGE;
	private boolean invincible = false;
	State enemyState;
	
	/**	Contructor
	 * initialises it's entity information, sets as a dormant state until the timer indicates it's ready to activate
	 * @param imageSrc the file path of its sprite
	 * @param xSpawn x spawn location
	 * @param ySpawn y spawn location
	 * @param speed move speed
	 * @param activateTime how long in milliseconds until it's active
	 * @param score used in subclasses for easily resetting the correct score to the type of enemy
	 * @throws SlickException Slick is great at errors yes
	 */
	public Enemy (String imageSrc, float xSpawn, float ySpawn, float speed, int activateTime) throws SlickException {
		super(imageSrc, xSpawn, ySpawn, speed);
		this.activateTime=activateTime;
		this.deactivate();
		enemyCount++;
	}
	public Enemy (String imageSrc, float xSpawn, float ySpawn, float speed, int activateTime, int score) throws SlickException {
		super(imageSrc, xSpawn, ySpawn, speed);
		this.activateTime=activateTime;
		this.deactivate();
		enemyCount++;
		this.score = score;
	}
	
	/** Updates the enemy, determines its state, and chooses behaviour accordingly
	 * public to force this update if necessary, and so subclasses can overload
	 * @param input collects input from the player/user keyboard
	 * @param delta Time passed since last frame (milliseconds).
	 */
	public void update(Input input, int delta) {
		// handles the enemy in different states
		// until the counter has reached the spawn time, it lays dormant
		switch (this.getState()) {
			case INACTIVE:
				setRender(false);
				activateCount+=World.getGameSpeed();
				if (activateCount>=activateTime) {
					this.activate();
				}
				break;
			case ACTIVE:
				this.activeUpdate(delta);
				break;
		}
		super.update(input, delta);
	}
	
	/* HELPER METHODS */
	
	// performs behaviour while in an active state. this is set to public in case it needs to be forced
	public void activeUpdate(int delta) {
		setRender(true);
		move(delta);
	}
	
	// enemy move
	private void move(int delta) {
		this.addY(BASE_ENEMY_SPEED*delta);
	}
	/* getters */
	public int getDamage() {
		return damage;
	}
	public int getActivateTime() {
		return activateTime;
	}
	public boolean isInvincible() {
		return invincible;
	}
	// returns the score
	public int getScore() {
		return score;
	}
	/* setters */
	public void setInvincibility(boolean i) {
		invincible = i;
	}
}
