import org.newdawn.slick.SlickException;
import org.newdawn.slick.Input;

import java.util.ArrayList;

/**
 * It's kind of a plasma laser whatever
 * hits things and deals damage if they're not invincible
 * and can't be destroyed unless it hits a target or goes offscreen
 */
public class Shot extends Entity{
	
	/* images */
	private static final String playerShotSprite = "res/shot.png";
	private static final String ENEMY_SHOT_SPRITE = "res/enemy-shot.png";
	
	/* flag for shot destroyed */
	public static final int BASE_SHOT_DAMAGE = -1;
	private static final float PLAYER_SHOT_SPEED = -3f;
	private static final float ENEMY_SHOT_SPEED = 0.7f;
	
	/* Array for collision detection */
	ArrayList<Entity> enemies = new ArrayList<>();
	
	/** creates a new shot with targets
	 * 
	 * @param x location
	 * @param y
	 * @param enemies targets for the projectile
	 * @throws SlickException
	 */
	public Shot (float x, float y, ArrayList<Entity> enemies) throws SlickException {
		super(playerShotSprite, x, y, PLAYER_SHOT_SPEED);
		this.enemies = enemies;
		// if it targets the player, change info for enemy shot
		if (getTarget() instanceof Player) {
			this.setSprite(ENEMY_SHOT_SPRITE);
			setSpeed(ENEMY_SHOT_SPEED);
			setInvincible(true);
		}
		ShotController.TOTAL_SHOTS++;
		this.activate();
	}
	// extra info for perhaps a specific projectile. template constructor
	public Shot (String imageSrc, float x, float y, float speed, ArrayList<Entity> enemies) throws SlickException {
		super(imageSrc, x, y, speed);
		this.enemies = enemies;
	}
	
	/**	Updates shot collision and location 
	 * 	@param input takes player input
	 * 	@param delat milliseconds each frame
	 */
	public void update(Input input, int delta) {
		// move shot up or down
		addY(this.getSpeed());
		super.update(input, delta);
		// Collision. if it connects with an enemy, destroy it, and set flag for self destruction
		for(int i=0; i<enemies.size(); i++) {
			if (contactOther(enemies.get(i)) && !enemies.get(i).isInvincible() && isActive()) {
				setHealth(0);
				FXController.createSparkFx(getX(), getY());
				ShotController.TOTAL_SHOTS--;
				// player has own separate collision mechanics
				if (!(enemies.get(i) instanceof Player)) {
					enemies.get(i).adjustHealth(BASE_SHOT_DAMAGE);
				}
				this.deactivate();
				break;
			}
		}
	}
	
	/* HELPER METHODS */
	
	/* Getters */
	public Entity getTarget() {
		if (enemies.size()>0) {
			return enemies.get(0);
		} else return null; 
	}
}

