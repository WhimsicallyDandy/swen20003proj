import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.geom.Rectangle;

/**
 *  controls User Interface elements, 
 *  reads information from other classes
 *  displays lives, score, boss healthbar
 */
public class UIController {
	
	// determines player lives display
	private final int PLAYER_LIVES_XPADDING = 20;
	private final int PLAYER_LIVES_YPADDING = 20;
	private final int SCORE_YPADDING = 20;
	private final int PLAYER_LIVES_GAP = 10;
	private final int BOSS_HEALTHBAR_WIDTH = 8;
	private final float PLAYER_LIVES_SCALE = 1f;
	private final String PLAYER_LIFE_SPRITE = "res/lives.png";
	private final Color BOSS_BAR_LEFT = new Color(255, 0, 0);
	private final Color BOSS_BAR_RIGHT = new Color(0, 255, 0);
	
	// handles score display and count
	private static int score;
	
	private Player player;
	private Image playerLifeSprite;
	private Graphics graphics = new Graphics();
	
	/** inits a new UI with fresh elements
	 * @param player the player. that's the user
	 * @throws SlickException
	 */
	public UIController (Player player) throws SlickException {
		this.player = player;
		playerLifeSprite = new Image(PLAYER_LIFE_SPRITE);
		score = 0;
	}
	
	/** Render all ui elements
	 * 
	 * @param g graphics for all your slick needs
	 */
	public void render(Graphics g) {
		// renders score
		g.drawString("Score: "+score, PLAYER_LIVES_XPADDING, App.SCREEN_HEIGHT-SCORE_YPADDING);
		// renders Player Lives display 
		for (int i=0; i<player.getHealth(); i++) {
			playerLifeSprite.draw(
					// positions the lives very specifically
					PLAYER_LIVES_XPADDING+i*(playerLifeSprite.getWidth()*PLAYER_LIVES_SCALE+PLAYER_LIVES_GAP),
					App.SCREEN_HEIGHT-(SCORE_YPADDING+PLAYER_LIVES_YPADDING+playerLifeSprite.getHeight()*PLAYER_LIVES_SCALE), 
					PLAYER_LIVES_SCALE);
		}
		// renders boss health
		if (BasicBoss.BOSS_SPAWNED) {
			drawBossHealthbar(g);
		}
	}
	
	/* HELPER METHODS */
	
	/** Renders a health bar at the top of the boss
	 * 
	 * @param g Slick drawing capabilities
	 */
	// base code from http://slick.ninjacave.com/forum/viewtopic.php?t=3364
	private void drawBossHealthbar(Graphics g) {
		// only draw if boss is existing and not null
		BasicBoss temp = World.getBoss();
		if (temp!= null) {
			// create left and top coords, and the length of the bar
			int healthbarLength = (int)temp.getBox().getWidth();
			int leftBound = (int)temp.getBox().getLeft();
			int topBound = (int)temp.getBox().getTop();
			// create new rectangle as backdrop for bar
			Rectangle bar = new Rectangle(
					leftBound, 
					topBound, 
					healthbarLength * temp.getHealth() / BasicBoss.BASIC_BOSS_HEALTH,
					BOSS_HEALTHBAR_WIDTH);
			// create the actual health bar with a gradient effect
	        GradientFill fill = new GradientFill(
	        		leftBound, 
	        		topBound, 
	        		// red
	        		BOSS_BAR_LEFT,
	                leftBound + healthbarLength, 
	                topBound + BOSS_HEALTHBAR_WIDTH,
	                // green
	                BOSS_BAR_RIGHT);
	        // draw the bar 
	        graphics.setColor(Color.darkGray);
	        graphics.fillRect(leftBound, topBound, healthbarLength, BOSS_HEALTHBAR_WIDTH);
	        graphics.fill(bar, fill); 
		}
	}
	
	// adds value to score
	public static void addScore(int num) {
		score+=num;
	}
	
}
